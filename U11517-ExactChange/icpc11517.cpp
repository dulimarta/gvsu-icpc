//
// Created by Hans Dulimarta on 10/20/17.
//

#include <sstream>
#include <iostream>
#include <string>
#include <map>
#include <vector>

using  namespace std;

const string test_data = "1\n"
        "17\n"
        "3\n"
        "1\n"
        "10\n"
        "20";

int numBills;
vector<int> numCoin;
vector<int> payWith;

int exact_change (int amtOwe, const vector<int>& bills) {
    if (amtOwe < 0) {
        return -amtOwe; /* amount of overpayment */
    }
    if (amtOwe == 0)
        return 0;
    if (numCoin[amtOwe] != -1)
        return numCoin[amtOwe];
    auto min_so_far = 100000;
    int min_pos;
    for (int k = 0; k < bills.size(); k++) {
        int cost;
        if (amtOwe < bills[k]) {
            payWith[amtOwe] = k;
        }
        cost = 1 + exact_change(amtOwe - bills[k], bills);
        if (cost < min_so_far) {
            min_so_far = cost;
            min_pos = k;
        }
    }
    numCoin[amtOwe] = min_so_far;
    payWith[amtOwe] = min_pos;
    return min_so_far;
}

void do_exact_change(istream& input) {
    int amtOwe;
    vector<int> bills;

    input >> amtOwe >> numBills;

    for (int k = 0; k < numBills; k++) {
        int val;
        input >> val;
        bills.push_back(val);
    }
    numCoin.resize(amtOwe + 1);
    payWith.resize(amtOwe + 1);
    fill(numCoin.begin(), numCoin.end(), -1);
    fill(payWith.begin(), payWith.end(), -1);
    numCoin[0] = 0;
    for (int v = 1; v <= amtOwe; v++)
        exact_change(v, bills);
    int count = 0, total = 0;
    while (amtOwe > 0) {
        count++;
        int bill = bills[payWith[amtOwe]];
        total += bill;
        amtOwe -= bill;
    }
    cout << total << ' ' << count << endl;
}

int main () {

#ifdef ONLINE_JUDGE
    istream& input = cin;
#else
    istringstream input(test_data);
#endif

    int numTests;
    input >> numTests;
    for (int k = 0; k < numTests; k++) {
        do_exact_change(input);
    }

}