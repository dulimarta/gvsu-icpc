#include <vector>
#include <iostream>
using namespace std;

int main () {
   int N;
   vector<unsigned long> sum (101);
   for (int k = 0; k < sum.size(); k++)
       sum[k] = k * k;
   for (int k = 1; k < sum.size() ; k++) 
       sum[k] = sum[k] + sum[k-1];
   cin >> N;
   while (N != 0) {
      cout << sum[N] << endl;
      cin >> N;
   }
}
