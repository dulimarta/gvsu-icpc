/*********************************************************
File Name:	 g.cpp
Author:	    	 Steven Hoffman
Creation Date: 03-12-2014
Description:	  
************************************************************/
#include <algorithm>
#include <list>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

bool comp (char a, char b) { return (a < b); }

int main(int argc, char* argv[]){

	char c;
	bool eof = false;
	while (!eof) {

		list<char> a;
		list<char> b;

		if ((c=getchar())== EOF) { return 0;}
		else if (c != '\n'){ 
			a.push_back(c);

			// read in a
			while ((c=getchar())!='\n') {
				a.push_back(c);
			}
		}

		// read in b
		while ((c=getchar())!='\n') {
			if (c==EOF) { eof=true; break;}
			b.push_back(c);
		}

		// sort
		//sort(a.begin(), a.end(), comp);
		//sort(b.begin(), b.end(), comp);
		a.sort();
		b.sort();

		list<char>::iterator ater, bter;
		ater = a.begin(); bter = b.begin();

		while (ater != a.end() && bter != b.end()) {
			
			if (*ater == *bter) {
				cout << *ater;
				++ater;
				++bter;
			}
			else if (*ater > *bter) {
				++bter;
			}
			else {
				++ater;
			}
		}
		cout << '\n';
	}

   return 0;
}
