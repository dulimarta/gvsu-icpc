/*********************************************************
File Name:	 g.cpp
Author:	    	 Steven Hoffman
Creation Date: 03-12-2014
Description:	  
************************************************************/
#include <algorithm>
#include <list>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

bool comp (char a, char b) { return (a < b); }

int main(int argc, char* argv[]){

	char c;
	bool eof = false;
	while (!eof) {

		list<char> a;
		list<char> b;

		if ((c=getchar())== EOF || c == '\n') { return 0;}
		else { a.push_back(c);}

		// read in a
		while ((c=getchar())!='\n') {
			a.push_back(c);
		}
		// read in b
		while ((c=getchar())!='\n') {
			if (c==EOF) { eof=true; break;}
			b.push_back(c);
		}

		// sort
		//sort(a.begin(), a.end(), comp);
		//sort(b.begin(), b.end(), comp);
		a.sort();
		b.sort();

		list<char> c;
		list<char>::iterator ater, bter;
		ater = a.begin(); bter = b.begin();

		while (ater != a.end() && bter != b.end()) {
			
			if (*ater == *bter) {
			//	c.push_back(*ater);
				cout << *ater;
				++ater;
				++bter;
			}
			else if (*ater > *bter) {
				++bter;
			}
			else {
				++ater;
			}
		}
		cout << '\n';
/*		ater = c.begin()
		while (ater != c.end()) {
			printf("%c", *ater++);
		} printf("\n");*/
	}

   return 0;
}
