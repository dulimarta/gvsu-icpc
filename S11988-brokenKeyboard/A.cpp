/*********************************************************
File Name:	 A.cpp
Author:	    	 Steven Hoffman
Creation Date: 02-19-2014
Description:	  
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <list>
#include <iostream>

using namespace std;

list<char> line;

int main(int argc, char* argv[]){

	list<char>::iterator iter = line.begin();
	++iter;
	char c;
	while ((c = getchar()) != EOF) {
		
		if (c == '[') {
			iter = line.begin();
		} else if (c == ']') {
			iter = line.end();
		} else if (c == '\n') {
			for (iter = line.begin(); iter != line.end(); ++iter) {
				cout << *iter;
			} cout << '\n';
			line.clear();
			iter = line.begin();
			++iter;
		} else {
			line.insert(iter, c);		
		}
	}
	

   return 0;
}
