//
// Created by Hans Dulimarta on 10/14/17.
//

#include <sstream>
#include <string>
#include <iostream>
#include <vector>
using namespace std;

const string test_data = "2\n"
        "4 3 0 0 0 23\n"
        "25\n"
        "100\n"
        "1 0 1\n"
        "1\n"
        "6";

long evalpoly(int n, vector<int>& coeff) {
    long val = 0;
    // Evaluate polynom using Horner's technique
    for (int k = coeff.size() - 1; k >= 0; k--)
        val = val * n + coeff[k];
    return val;
}

int main() {
#ifdef ONLINE_JUDGE
    istream& input = cin;
#else
    istringstream input(test_data);
#endif
    int ntests;
    input >> ntests;

    for (int k = 0; k < ntests; k++) {
        string polycoeffs;
        int repeat_count, term_pos, polydegree;
        vector<int> coeffs;
        input >> polydegree;
        int coeff;
        for (int m = 0; m < polydegree + 1; m++) {
            input >> coeff;
            coeffs.push_back(coeff);
        }
        input >> repeat_count >> term_pos;
        int term_count = 0;
        int d = repeat_count;
        int n = 1;
        while (term_count < term_pos) {
            term_count += d;
            d += repeat_count;
            n++;
        }
        n--;
        cout << evalpoly(n, coeffs) << endl;
    }
    return 0;
}