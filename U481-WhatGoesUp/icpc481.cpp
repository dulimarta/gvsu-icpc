//
// Created by Hans Dulimarta on 10/20/17.
//
// Verdict: Time Limit Exceeded

#include <sstream>
#include <string>
#include <iostream>
#include <vector>
#include <stack>

using namespace std;
const string test_data1 = "-7\n"
        "10\n"
        "9\n"
        "2\n"
        "3\n"
        "8\n"
        "8\n"
        "6";

const string test_data = "0 0 0 0 0 2 6 7 7 7 9";

int main() {
#ifdef ONLINE_JUDGE
    istream& input = cin;
#else
    istringstream input(test_data);
#endif
    vector<int> nums;
    int val;
    while (input >> val)
        nums.push_back(val);

    vector<int> lis (nums.size());
    vector<int> from (nums.size());
    lis[0] = 1;
    from[0] = -1;
    int pos_global_max = 0;
    for (int k = 1; k < nums.size(); k++) {
        int max = 0;
        int posmax = -1;
        for (int m = k - 1; m >= 0; m--)
            if (nums[k] > nums[m]) {
                if (max < lis[m]) {
                    max = lis[m];
                    posmax = m;
                }
            }
        lis[k] = max + 1;
        from[k] = posmax;
        if (lis[k] > lis[pos_global_max])
            pos_global_max = k;
    }

    stack<int> pos_stack;
    int pos = pos_global_max;
    while (pos >= 0) {
        pos_stack.push(nums[pos]);
        pos = from[pos];
    }
    cout << pos_stack.size() << endl << "-" << endl;
    while (pos_stack.size() > 0) {
        cout << pos_stack.top() << endl;
        pos_stack.pop();
    }
    return 0;
}