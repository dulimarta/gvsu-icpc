//
// Created by Hans Dulimarta on 10/12/17.
//

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

const string test_data = "7 1 2 3 4 5 6 7\n"
        "8 1 2 3 5 8 13 21 34\n"
        "0";
const int LOTTO_LEN = 6;

int main() {
#ifndef ONLINE_JUDGE
    istringstream input(test_data);
#else
    istream& input = cin;
#endif
    int subset_size;
    input >> subset_size;
    while (subset_size != 0) {
        vector<int> numbers;
        int n;
        for (int k = 0; k < subset_size; k++) {
            input >> n;
            numbers.push_back(n);
        }
        const int N = numbers.size();
        for (auto i1 = 0; i1 < N - 5; i1++) {
            for (auto i2 = i1 + 1; i2 < N - 4; i2++) {
                for (auto i3 = i2 + 1; i3 < N - 3; i3++) {
                    for (auto i4 = i3 + 1; i4 < N - 2; i4++) {
                        for (auto i5 = i4 + 1; i5 < N - 1; i5++) {
                            for (auto i6 = i5 + 1; i6 < N; i6++) {
                                printf("%d %d %d %d %d %d\n", numbers[i1], numbers[i2],
                                       numbers[i3], numbers[i4], numbers[i5], numbers[i6]);
                            }
                        }
                    }
                }
            }
        }


        input >> subset_size;
        if (subset_size != 0)
            cout << endl;                 /* newline between test cases */
    }

}