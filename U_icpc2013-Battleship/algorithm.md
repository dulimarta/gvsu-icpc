ALGORITHM
=========
(1) Rule out all rows/columns with zero hint.
These cells must be occupied by a portion of a ship.
    (3a) Allocating a cell to a portion of a ship forces the surrounding
         cells to be FREE
(4) Update row/columns hints affected by step (3) [lower hint numbers ]
(5) Apply recursive backtracking algorithm by placing the largest ship
    first

DATA STRUCTURES
===============
(a) Stack of {list of open spaces}
    each node in open save (row, col, orientation, number of open cells)

    Begin 20 open spaces
     (1, 1, H, 10)
     (2, 1, H, 10)
      ....
    (10, 1, H, 10)
     (1, 1, V, 10)
     (1, 2, V, 10)
    ...
     (1, 10, V, 10)

(b) Stack of {list of ship placements}
    each of ship placement (row, col, orientazion, size of ship)

(c) Array of 4 integers: number of unallocated ships

Recursive to place a ship (start from the largest), each time update
the two lists above.
When the recursive call finds a solution to place all the 10 ships but
the list of open spaces is NOT empty, this placement is ambiguous and
another branch in the placement tree should give another solution.

Resolution of ambiguity: find the common parent of two possible solutions.
The immediate children of this common parent should hold the information
needed to disambiguate the ship placement.
