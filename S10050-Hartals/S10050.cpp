//  S10050.cpp
//
//
//  Created by Joshua Engelsma on 3/19/14.
//
//
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
using namespace std;

int main(int argc, const char * argv[])
{
    int numberOfInput, timePeriod, hartelParameter, nbrPoliticalParties;
    cin >> numberOfInput;
    //loop through all input chunks
    for(int i=0; i<numberOfInput; i++){
        cin >> timePeriod;
        cin >> nbrPoliticalParties;
        vector<bool> strikes (timePeriod + 1, false);
        for(int index = 0; index < nbrPoliticalParties; index++){
            int dayToStrike;
            cin >> dayToStrike;
            for(int col = dayToStrike ; col <= timePeriod; col+= dayToStrike){
                if (col % 7 == 6) continue; /* skip Fridays */
                if (col % 7 == 0) continue; /* skip Saturdays */
                strikes[col] = true;
            }
        }
        int result = count (strikes.begin(), strikes.end(), true);
        cout << result << endl;
    }
    
    return 0;
}
