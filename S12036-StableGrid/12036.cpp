//
//  main.cpp
//  Programming Contest
//
//  Created by Joshua Engelsma on 3/12/14.
//  Copyright (c) 2014 Joshua Engelsma. All rights reserved.
//

#include <iostream>
#include <iomanip>
#include <fstream>
#include<vector>
using namespace std;

int main(int argc, const char * argv[])
{

    //std::ifstream input (argv[1]);
    int numberOfCol, numberOfInput, data;
    cin >> numberOfInput;
    //loop for all blocks of input
    for(int i = 0; i < numberOfInput; i++){
        cin >> numberOfCol;
        vector<int> numberCounts(100, 0);
        string output = "yes";
        //loop for size of matrix
        for(int row = 0; row<(numberOfCol*numberOfCol); row++){
            cin >> data;
            numberCounts[data]++;
            if(numberCounts[data] > numberOfCol){
                output = "no";
            }
        }
        cout << "Case " << (i+1) << ": " <<output << endl;
    }
    return 0;
}
