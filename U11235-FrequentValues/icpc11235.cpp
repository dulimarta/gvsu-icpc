//
// Created by Hans Dulimarta on 10/12/17.
//
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <iostream>

using namespace std;

const string test_data =
        "10 3\n"
        "-1 -1 1 1 1 1 3 10 10 10\n"
        "2 3\n"
        "1 10\n"
        "5 10\n"
        "0";

int find_max (int lb, int rb, vector<int>& arr) {
    int maxval = 1;
    lb += arr.size()/2;
    rb += arr.size()/2;
    while (lb < rb) {
        if (lb % 2 == 1) {
            maxval = std::max(maxval, arr[lb]);
            lb++;
        }
        if (rb % 2 == 1) {
            rb--;
            maxval = std::max(maxval, arr[rb]);
        }
        lb /= 2;
        rb /= 2;
    }
    return maxval;
}

int main() {
#ifndef ONLINE_JUDGE
    istringstream input(test_data);
#else
    istream& input = cin;
#endif
    int n, q;
    input >> n;
    while (n != 0) {
        vector<map<int,int>> itemCount (2 * n);
        vector<int> freq (2 * n);
        input >> q;
        /* read in the numbers */
        for (int k = 0; k < n; k++) {
            int a;
            input >> a;
            itemCount[n + k].insert (make_pair(a,1));
            freq[n + k] = 1;
        }
        /* build segment tree */
        for (int start = n - 1; start >= 1; start--) {
            int lc = 2 * start;
            int rc = lc + 1;
            auto& map1 = itemCount[lc];
            for (auto& kv : itemCount[lc]) {
                itemCount[start].insert(kv);
            }
            for (auto& kv : itemCount[rc]) {
                itemCount[start][kv.first] += kv.second;
            }
            int max = 0;
            for (auto& kv : itemCount[start]) {
                if (max < kv.second)
                    max = kv.second;
            }
            freq[start] = max;
        }

        for (int k = 0; k < q; k++) {
            int low, hi;
            input >> low >> hi;
            cout << find_max (low - 1, hi, freq) << endl;
        }
        input >> n;
    }
}